using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Avico.DTO
{
    /// <summary>
    /// Contact DTO
    /// </summary>
    public class Contacts
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Prenom { get; set; }
        [Required]
        public string Nom { get; set; }
        public List<Adresse> Adresses { get; set; }
        public List<Telephone> Telephones { get; set; }
    }
}