using System.Net;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Avico;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Avico.DTO;
using AspNetCore.Http.Extensions;
using Avico.Controllers;
using System.Collections.Generic;

namespace ContactManager.FunctionnalTests
{
    public class ContactControllerTest: Test
    {
        public ContactControllerTest(): base()
        {
            ContactsController.contacts = new List<Contacts>{
                new Contacts {
                    Id = 1,
                    Nom = "DUPUYS",
                    Prenom = "Thomas",
                    Adresses = new List<Adresse> {
                        new Adresse {
                            Id = 1,
                            NumeroRue = 58,
                            NomRue = "rue des lilats",
                            NomVille = "Paris",
                            CodePostal = 75000,
                            ContactId = 1
                        },
                        new Adresse {
                            Id = 2,
                            NumeroRue = 75,
                            NomRue = "rue de trois fleurs",
                            NomVille = "Paris",
                            CodePostal = 75000,
                            ContactId = 1
                        }
                    },
                    Telephones = new List<Telephone> {
                        new Telephone {
                            Id = 1,
                            NumeroTelephone = 0658239625,
                            ContactId = 1
                        },
                        new Telephone {
                            Id = 2,
                            NumeroTelephone = 0658639625,
                            ContactId = 1
                        }
                    },
                },

                new Contacts {
                    Id = 2,
                    Nom = "DURANT",
                    Prenom = "Charles",
                    Adresses = null,
                    Telephones = null
                },

                new Contacts {
                    Id = 3,
                    Nom = "PESQUET",
                    Prenom = "Thomas",
                    Adresses = null,
                    Telephones = null
                },

                new Contacts {
                    Id = 4,
                    Nom = "WATSON",
                    Prenom = "Emma",
                    Adresses = new List<Adresse> {
                        new Adresse {
                            Id = 3,
                            NumeroRue = 58,
                            NomRue = "rue des lilats",
                            NomVille = "Paris",
                            CodePostal = 75000,
                            ContactId = 1
                        }
                    },
                    Telephones = new List<Telephone> {
                        new Telephone {
                            Id = 3,
                            NumeroTelephone = 0658239625,
                            ContactId = 1
                        }
                    },
                }
            };
        }

        [Fact]
        public async Task TestGetContactAsync()
        {
            var response = await _testClient.GetAsync("/api/contacts");
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var contacts = ContactsController.contacts;
            ContactsController.contacts = new List<Contacts>(){};
            response = await _testClient.GetAsync("/api/contacts");
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            ContactsController.contacts = contacts;
        }

        [Fact]
        public async Task TestGetContactByIdAsync(){
            var response = await _testClient.GetAsync("/api/contacts/" + 1);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            response = await _testClient.GetAsync("/api/contacts/" + int.MaxValue);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutContactAsync()
        {
            var contact = new Contacts{
                Id = 2,
                Nom = "RAOULT",
                Prenom = "Didier",
                Adresses = null,
                Telephones = null
            };
            var response = await _testClient.PutAsJsonAsync("/api/contacts/" + contact.Id, contact);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            response = await _testClient.PutAsJsonAsync("/api/contacts/" + 1, contact);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            contact.Id = int.MaxValue;
            response = await _testClient.PutAsJsonAsync("/api/contacts/" + contact.Id, contact);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            contact.Id = 2;
        }

        [Fact]
        public async Task TestDeleteContactAsync()
        {
            var response = await _testClient.DeleteAsync("/api/contacts/" + int.MaxValue);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            response = await _testClient.DeleteAsync("/api/contacts/" + 3);
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [Fact]
        public async Task TestGetAdressesByContactIdAsync(){
            var response = await _testClient.GetAsync("/api/contacts/" + 1 + "/adresses");
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            response = await _testClient.GetAsync("/api/contacts/" + 2 + "/adresses");
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutAdressesAsync()
        {
            var adresse = new Adresse{
                Id = 1,
                NumeroRue = 17,
                NomRue = "rue des Tests",
                NomVille = "Cognac",
                CodePostal = 16100,
                ContactId = 1
            };

            var response = await _testClient.PutAsJsonAsync("/api/contacts/" + adresse.ContactId + "/adresses/" + 2, adresse);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            response = await _testClient.PutAsJsonAsync("/api/contacts/" + int.MaxValue + "/adresses/" + adresse.Id, adresse);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            adresse.Id = int.MaxValue;
            response = await _testClient.PutAsJsonAsync("/api/contacts/" + adresse.ContactId + "/adresses/" + adresse.Id, adresse);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            adresse.Id = 1;

            response = await _testClient.PutAsJsonAsync("/api/contacts/" + adresse.ContactId + "/adresses/" + adresse.Id, adresse);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task TestDeleteAdressesAsync()
        {
            var response = await _testClient.DeleteAsync("/api/contacts/" + int.MaxValue + "/adresses/" + 1);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            response = await _testClient.DeleteAsync("/api/contacts/" + 4 + "/adresses/" + int.MaxValue);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            response = await _testClient.DeleteAsync("/api/contacts/" + 4 + "/adresses/" + 3);
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [Fact]
        public async Task TestGetTelephonesByContactIdAsync(){
            var response = await _testClient.GetAsync("/api/contacts/" + 1 + "/telephones");
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            response = await _testClient.GetAsync("/api/contacts/" + 2 + "/telephones");
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestPutTelephonesAsync()
        {
            var telephone = new Telephone{
                Id = 1,
                NumeroTelephone = 0658545191,
                ContactId = 1
            };

            var response = await _testClient.PutAsJsonAsync("/api/contacts/" + 1 + "/telephones/" + 2, telephone);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            response = await _testClient.PutAsJsonAsync("/api/contacts/" + int.MaxValue + "/telephones/" + telephone.Id, telephone);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            telephone.Id = int.MaxValue;
            response = await _testClient.PutAsJsonAsync("/api/contacts/" + 1 + "/telephones/" + telephone.Id, telephone);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            telephone.Id = 1;

            response = await _testClient.PutAsJsonAsync("/api/contacts/" + 1 + "/telephones/" + 1, telephone);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task TestDeleteTelephonesAsync()
        {
            var response = await _testClient.DeleteAsync("/api/contacts/" + int.MaxValue + "/telephones/" + 1);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            response = await _testClient.DeleteAsync("/api/contacts/" + 4 + "/telephones/" + int.MaxValue);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            response = await _testClient.DeleteAsync("/api/contacts/" + 4 + "/telephones/" + 3);
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }
    }
}
