using System;
using System.Net.Http;
using System.Threading.Tasks;
using Avico;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace ContactManager.FunctionnalTests
{
    public class Test
    {
        protected readonly TestServer _testServer;
        protected readonly HttpClient _testClient;
        public Test()
        {
            //Initializing the test environment
            _testServer = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            //Test client creation
            _testClient = _testServer.CreateClient();
        }
    }
}