using System;
using Xunit;
using Avico.DTO;
using FluentAssertions;
using System.Collections.Generic;

namespace ContactManager.UnitTests
{
    public class AdresseDtoTest
    {
        [Fact]
        public void AdresseTest()
        {
            Action act = () => {
                var contact = new Contacts{Id = 1};
                var adresse = new Adresse{
                    Id = 1,
                    NumeroRue = 14,
                    NomRue = "Ma rue",
                    NomVille = "Cognac",
                    CodePostal = 16200,
                    ContactId = contact.Id,
                    Contact = contact
                };
            };

            act.Should().NotThrow();
        }
    }
}
