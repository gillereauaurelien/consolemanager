using System;
using System.Collections.Generic;
using Avico.DTO;
using FluentAssertions;
using Xunit;

namespace ContactManager.UnitTests
{
    public class ContactsDtoTest
    {
        [Fact]
        public void ContactsTest()
        {
            Action act = () => {
                var contact = new Contacts{
                    Id = 1,
                    Nom = "PESQUET",
                    Prenom = "Thomas"    
                };

                contact.Adresses = new List<Adresse>{
                    new Adresse{Id = 1, ContactId = contact.Id, Contact = contact}
                };

                contact.Telephones = new List<Telephone>{
                    new Telephone{Id = 1, ContactId = contact.Id, Contacts = contact}
                };
            };

            act.Should().NotThrow();
        }
    }
}