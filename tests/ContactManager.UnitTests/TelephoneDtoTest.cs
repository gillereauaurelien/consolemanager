using System;
using Avico.DTO;
using FluentAssertions;
using Xunit;

namespace ContactManager.UnitTests
{
    public class TelephoneDtoTest
    {
        [Fact]
        public void TelephoneTest()
        {
            Action act = () => {
                var contact = new Contacts{Id = 1};
                var telephone = new Telephone{
                    Id = 1,
                    NumeroTelephone = 0658545191,
                    ContactId = contact.Id,
                    Contacts = contact
                };
            };

            act.Should().NotThrow();
        }
    }
}